#include "RAK811.h"
#include "SoftwareSerial.h"
#define WORK_MODE LoRaWAN   //  LoRaWAN or LoRaP2P
#define JOIN_MODE ABP    //  OTAA or ABP
#if JOIN_MODE == OTAA
String DevEui = "2222222222222222";
String AppEui = "AAAAAAAAAAAAAAAA";
String AppKey = "01020304050607080910111213141516";
#else JOIN_MODE == ABP
String NwkSKey = "01020304050607080910111213141516";
String AppSKey = "01020304050607080910111213141516";
String DevAddr = "12345678";
#endif
#define TXpin 9   // Set the virtual serial port pins
#define RXpin 10
SoftwareSerial mySerial(RXpin,TXpin);    // Declare a virtual serial port
char* buffer = "72616B776972656C657373";

RAK811 RAKLoRa(mySerial);
#define LED 13

void setup() {
 Serial.begin(115200);
 while(Serial.read()>= 0) {}
 while(!Serial);
 Serial.println("StartUP");

 pinMode(LED, OUTPUT);

 mySerial.begin(115200);

}

void loop() {
 
 //Serial.println(RAKLoRa.rk_getVersion());
 //delay(500);
 //Serial.println(RAKLoRa.rk_getBand());
 //delay(500);
 if(RAKLoRa.rk_setWorkingMode(WORK_MODE))
 {
  Serial.println("Set mode OK!");
  if (RAKLoRa.rk_initABP(DevAddr,NwkSKey,AppSKey))
    {
      Serial.println("Init ABP OK!");
      if (RAKLoRa.rk_joinLoRaNetwork(JOIN_MODE))
       {
          Serial.println("Join OK!");

          //Serial.println("Set retrans");
          RAKLoRa.rk_setConfig("retrans", "1");  
                  
          //Serial.println("Set dr");
          RAKLoRa.rk_setConfig("dr", "8");
          
          //Serial.println("Set tx_power");
          RAKLoRa.rk_setConfig("tx_power", "20");
          
          //Serial.println("Set duty");
          RAKLoRa.rk_setConfig("duty", "off");


//          static bool setted = false;
//          if( setted == false ) {
//            for(int i = 8; i < 63; i++) {
//              String index = String(i);
//              String value = index + String(",off,0,3");
//              RAKLoRa.rk_setConfig("ch_list", value);
//              Serial.println(value);
//              setted = true;
//              delay(500);
//            }
//          }
          
          //Serial.println("Get retrans: ");
          //Serial.println(RAKLoRa.rk_getConfig("retrans"));
          
          //Serial.println("Get rx2: ");
          //Serial.println(RAKLoRa.rk_getConfig("rx2"));
          
          //Serial.println("Get ch_list: ");
          //Serial.println(RAKLoRa.rk_getConfig("ch_list"));

          mySerial.println("at+send=1,1,72616B776972656C657373");
          delay(500);
          mySerial.setTimeout(2000);
          String recv = mySerial.readStringUntil('\n');
          if( recv.indexOf("OK") != -1 ) {
            Serial.println("Send Data OK!");
            digitalWrite(LED, 1);

            unsigned long time = millis();
            while( (mySerial.available() < 1) && (((unsigned long)millis() - time) < 25000) );
            
            String recv = mySerial.readStringUntil('\n');
            Serial.print('[');  Serial.print(recv); Serial.println(']');
            
            if( (recv.indexOf("1") != -1) || (recv.indexOf("0,0,") != -1) ) {      
          
              digitalWrite(LED, 0);
              Serial.println("Recv something");
              Serial.print("Get Status: ");
              Serial.println(RAKLoRa.rk_checkStatusStatistics());
              
            }
                    
          }
          else {
            Serial.println("Send Data Fail!");
          }            
          delay(2000);
           
        }
     }
 }
 delay(500);
}

//at+mode=0
//at+set_config=dev_addr:2604195E&nwks_key:CBF42B80A3103CC4DF8471C23F90B0B9&apps_key:73AB8EAB4D5BD7EBFB34F8820C750140&tx_power:20&duty:off&retrans:1
////at+set_config=dev_addr:2604195E&nwks_key:01020304050607080910111213141516&apps_key:01020304050607080910111213141516&tx_power:20&duty:off&retrans:1
//at+set_config=ch_list:8,off&ch_list:9,off&ch_list:10,off&ch_list:11,off&ch_list:12,off&ch_list:13,off&ch_list:14,off&ch_list:15,off&ch_list:16,off&ch_list:17,off&ch_list:18,off&ch_list:19,off&ch_list:20,off&ch_list:21,off&ch_list:22,off&ch_list:23,off
//at+set_config=ch_list:24,off&ch_list:25,off&ch_list:26,off&ch_list:27,off&ch_list:28,off&ch_list:29,off&ch_list:30,off&ch_list:31,off&ch_list:32,off&ch_list:33,off&ch_list:34,off&ch_list:35,off&ch_list:36,off&ch_list:37,off&ch_list:38,off&ch_list:39,off
//at+set_config=ch_list:40,off&ch_list:41,off&ch_list:42,off&ch_list:43,off&ch_list:44,off&ch_list:45,off&ch_list:46,off&ch_list:47,off&ch_list:48,off&ch_list:49,off&ch_list:50,off&ch_list:51,off&ch_list:52,off&ch_list:53,off&ch_list:54,off&ch_list:55,off
//at+set_config=ch_list:56,off&ch_list:57,off&ch_list:58,off&ch_list:59,off&ch_list:60,off&ch_list:61,off&ch_list:62,off&ch_list:63,off
//at+join=abp
//at+send=1,1,72616B776972656C657373



