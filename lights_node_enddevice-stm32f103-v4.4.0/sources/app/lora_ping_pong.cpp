#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_io.h"
#include "../common/xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "loramac/crypto/aes.h"
#include "loramac/crypto/cmac.h"

#include "lora_ping_pong.h"

static RadioEvents_t pingpong_events;
static void on_ping_pong_tx_done( void );
static void on_ping_pong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void on_ping_pong_tx_timeout( void );
static void on_ping_pong_rx_timeout( void );
static void on_ping_pong_rx_error( void );

void lora_ping_pong_init() {
	APP_DBG("lora_pingpong_init\n");

	pingpong_events.TxDone =	on_ping_pong_tx_done;
	pingpong_events.RxDone =	on_ping_pong_rx_done;
	pingpong_events.TxTimeout = on_ping_pong_tx_timeout;
	pingpong_events.RxTimeout = on_ping_pong_rx_timeout;
	pingpong_events.RxError =	on_ping_pong_rx_error;

	Radio.Init( &pingpong_events );
	Radio.SetChannel( SINGLE_CHANNEL_GW_FREQ );
	Radio.SetPublicNetwork(true);

	Radio.SetTxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_TX_POWER, 0, SINGLE_CHANNEL_GW_BANDWIDTH,
					   SINGLE_CHANNEL_GW_SF, SINGLE_CHANNEL_GW_CD,
					   SINGLE_CHANNEL_GW_PREAMBLE_LENGTH, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   true, 0, 0, SINGLE_CHANNEL_GW_IQ_TX, SINGLE_CHANNEL_GW_TX_TIMEOUT );

	Radio.SetRxConfig( MODEM_LORA, SINGLE_CHANNEL_GW_BANDWIDTH, SINGLE_CHANNEL_GW_SF,
					   SINGLE_CHANNEL_GW_CD, 0, SINGLE_CHANNEL_GW_PREAMBLE_LENGTH,
					   SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT, SINGLE_CHANNEL_GW_FIX_LENGTH_PAY,
					   0, true, 0, 0, SINGLE_CHANNEL_GW_IQ_RX, true );

	//timer_set(AK_TASK_UI_ID, AK_PINGPONG_SENT_TEST, 1, TIMER_ONE_SHOT);
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void lora_ping_pong_send() {
	APP_DBG("lora_pingpong_send\n");

	if(Radio.GetStatus() != RF_TX_RUNNING) {
		const uint8_t b_size = 100;
		uint8_t buf[b_size] = {0x11, 0x22, 0x33, 0x44};
		Radio.Send(buf, b_size);
	}
}

void on_ping_pong_tx_done( void )
{
	APP_DBG("on_tx_done\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
	lora_ping_pong_send();
}

void on_ping_pong_tx_timeout( void )
{
	APP_DBG("on_tx_timeout\n");
	//Radio.Sleep( );
	//Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
	lora_ping_pong_send();
}

void on_ping_pong_rx_done( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	APP_DBG("on_rx_done-size:%d, rssi:%d, snr:%d\n\n", size, rssi, snr);
	for(int i = 0; i< size; i++)
		APP_DBG("x%02X ", *(payload+i));
	APP_DBG("\n");

	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );

}

void on_ping_pong_rx_timeout( void )
{
	APP_DBG("on_rx_timeout\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}

void on_ping_pong_rx_error( void )
{
	APP_DBG("on_rx_error\n");
	//Radio.Sleep( );
	Radio.Rx( SINGLE_CHANNEL_GW_RX_TIMEOUT );
}
