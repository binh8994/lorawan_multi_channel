#include <SoftwareSerial.h>
#define LED 13

SoftwareSerial LoraSerial(5, 6); // RX, TX

//String devaddr = "017979FD";
//String nwkskey = "01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16";
//String appskey = "01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16";

String devaddr = "26041A31";
String nwkskey = "7FA0B1EBFC36CB91EDC821D1FCD495A3";
String appskey = "36509D4C207AA9B759C14D50261E9E1A";

String response;
String cmd;

void setup() {
  Serial.begin(9600);
  while (!Serial);
  LoraSerial.begin(9600);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, 0);

  while(LoraSerial.available())
    LoraSerial.read();

  cmd = "AT+DR=AS923";
  sendRawcmd(cmd);
  
  cmd = "AT+MODE=LWABP";
  sendRawcmd(cmd);
  
  cmd = "AT+ADR=OFF";
  sendRawcmd(cmd);

  cmd = "AT+CH=NUM, 0-7, 64-71";
  sendRawcmd(cmd);

  cmd = "AT+POWER=30";
  sendRawcmd(cmd);

  cmd = "AT+RETRY=1";
  sendRawcmd(cmd);

  cmd = "AT+KEY=NWKSKEY, \"" + nwkskey + "\"";
  sendRawcmd(cmd);

  cmd = "AT+KEY=APPSKEY, \"" + appskey + "\"";
  sendRawcmd(cmd);

  cmd = "AT+ID=DevAddr, \"" + devaddr + "\"";
  sendRawcmd(cmd);

  cmd = "AT+ID=DevEui";
  sendRawcmd(cmd);

  cmd = "AT+ID=AppEui";
  sendRawcmd(cmd);

  cmd = "AT+CMSGHEX=\"00000000\"";
  
}

void loop() {
  Serial.println("[Send]");
  digitalWrite(LED, 1);
  LoraSerial.println(cmd);
  
  uint32_t time = millis();
  while( ((uint32_t)millis() - time) < 10000 ) {
    response = LoraSerial.readStringUntil('\n');    
    Serial.println(response);    
    response += '\n';
    
    if(response.indexOf("SNR")!=-1) {      
      digitalWrite(LED, 0);

      int id1, id2;
      id1 = response.indexOf("RSSI");
      if(id1 != -1) {
        id2 = response.indexOf(",", id1 + 5);
        if(id2 != -1) {
          String rssi = response.substring(id1 + 5, id2) ;
          //Serial.println(rssi.toInt());
          id1 = id2 + 6;
          id2 = response.indexOf("\n", id1 + 1);
          if(id2 != -1) {
            String snr = response.substring(id1, id2) ;
            //Serial.println(snr.toInt());

            String msg = String(rssi.toInt(), DEC) + String(snr.toInt(), DEC);
            msg.replace('-', 'F');
            if(msg.length() % 2 != 0){
              msg += 'F';
            }
            cmd = "AT+CMSGHEX=\"" +  msg + "\"";
            Serial.println(cmd);
          }
          
        }        
      }
      
      break;
    }
  }
  
  while(LoraSerial.available()) LoraSerial.read();

  delay(10000);
}

String sendRawcmd(String s) {
  LoraSerial.println(s);
  String r = LoraSerial.readStringUntil('\n');
  Serial.println(r);
  delay(100);
  while(LoraSerial.available()) LoraSerial.read();
    return r;
}

