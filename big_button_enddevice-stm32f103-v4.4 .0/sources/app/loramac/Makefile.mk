CFLAGS        += -I./sources/app/loramac
CFLAGS		+= -I./sources/app/loramac/crypto
CFLAGS        += -I./sources/app/loramac/mac
CFLAGS        += -I./sources/app/loramac/region
CFLAGS        += -I./sources/app/loramac/radio
CFLAGS        += -I./sources/app/loramac/radio/sx1276

VPATH += sources/app/loramac
VPATH += sources/app/loramac/crypto
VPATH += sources/app/loramac/mac
VPATH += sources/app/loramac/radio
VPATH += sources/app/loramac/radio/sx1276
VPATH += sources/app/loramac/region

SOURCES += sources/app/loramac/crypto/aes.c
SOURCES += sources/app/loramac/crypto/cmac.c

SOURCES += sources/app/loramac/region/Region.c
SOURCES += sources/app/loramac/region/RegionCommon.c
SOURCES += sources/app/loramac/region/RegionEU433.c
#SOURCES += sources/app/loramac/region/RegionUS915.c
#SOURCES += sources/app/loramac/region/RegionAS923.c
#SOURCES += sources/app/loramac/region/RegionUS915-Hybrid.c
#SOURCES += sources/app/loramac/region/RegionAU915.c
#SOURCES += sources/app/loramac/region/RegionEU868.c
#SOURCES += sources/app/loramac/region/RegionCN470.c
#SOURCES += sources/app/loramac/region/RegionIN865.c
#SOURCES += sources/app/loramac/region/RegionCN779.c
#SOURCES += sources/app/loramac/region/RegionKR920.c
SOURCES += sources/app/loramac/mac/LoRaMac.c
SOURCES += sources/app/loramac/mac/LoRaMacCrypto.c
SOURCES += sources/app/loramac/radio/sx1276/sx1276.c
SOURCES += sources/app/loramac/radio/sx1276/sx1276_cfg.c


