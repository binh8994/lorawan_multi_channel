#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../driver/button/button.h"
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_ui.h"
#include "lorawan.h"
#include "lora_ping_pong.h"
#include "app_bsp.h"


void task_ui(ak_msg_t* msg) {
	switch (msg->sig) {
	case AK_BUTTON_ONE_CLICK:

		button_disable(&button);
		lorawan_send();
		button_enable(&button);

		break;

	case AK_BUTTON_LONG_PRESS:

		//button_disable(&button);
		//lorawan_leave();
		//button_enable(&button);

		sys_ctrl_reset();

		break;

	case AK_SENT_LEAVE_NETWORK:

		//sys_ctrl_reset();

		break;

	case AK_PINGPONG_SENT_TEST:

		lora_ping_pong_send();

		break;

	case AK_LORAWAN_SENT_TEST:

		lorawan_send();

		break;

	default:
		break;
	}
}
