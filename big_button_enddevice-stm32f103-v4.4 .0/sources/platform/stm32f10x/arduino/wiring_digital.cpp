#include "Arduino.h"
#include "../io_cfg.h"
#include "../../../sys/sys_dbg.h"

void pinMode(uint8_t pin, uint8_t mode) {
	switch (pin) {

	default:
		FATAL("AR", 0xF1);
		break;
	}
}

void digitalWrite(uint8_t pin, uint8_t val) {
	switch (pin) {

	default:
		FATAL("AR", 0xF2);
		break;
	}
}

int digitalRead(uint8_t pin) {
	int val = 0;
	switch (pin) {

	default:
		FATAL("AR", 0xF3);
		break;
	}
	return val;
}
