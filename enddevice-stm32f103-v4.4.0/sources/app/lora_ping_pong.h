#ifndef __LORA_PING_PONG_H__
#define __LORA_PING_PONG_H__

extern void lora_ping_pong_init();
extern void lora_ping_pong_send();

#endif // __LORA_PING_PONG_H__
