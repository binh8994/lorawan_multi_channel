/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: LoRa MAC layer global definitions

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#ifndef __LORAMAC_BOARD_H__
#define __LORAMAC_BOARD_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * Returns individual channel mask
 *
 * \param[IN] channelIndex Channel index 1 based
 * \retval channelMask
 */
#define DelayMs		sys_ctrl_delay_ms
#define MIN( a, b ) ( ( ( a ) < ( b ) ) ? ( a ) : ( b ) )
#define MAX( a, b ) ( ( ( a ) > ( b ) ) ? ( a ) : ( b ) )
#define POW2( n ) ( 1 << n )

#define LORAWAN_DEFAULT_DATARATE                    SINGLE_CHANNEL_GW_DR

/* binhnt61 */
/* BEGIN */
#define SINGLE_CHANNEL_GW_FREQ						433175000
#define SINGLE_CHANNEL_GW_BANDWIDTH					0
// 0: 125 kHz
// 1: 250 kHz
// 2: 500 kHz
// 3: Reserved
#define SINGLE_CHANNEL_GW_SF						12
// [SF7..SF12]
#define SINGLE_CHANNEL_GW_DR						0//DR_0
#define SINGLE_CHANNEL_GW_CD						1
// 1: 4/5
// 2: 4/6
// 3: 4/7
// 4: 4/8
#define SINGLE_CHANNEL_GW_TX_POWER					14	// dBm
//#define SINGLE_CHANNEL_GW_TX_POWER					30	// dBm
#define SINGLE_CHANNEL_GW_PREAMBLE_LENGTH			8
#define SINGLE_CHANNEL_GW_SYMBOL_TIMEOUT			5
#define SINGLE_CHANNEL_GW_FIX_LENGTH_PAY			false
#define SINGLE_CHANNEL_GW_IQ_TX						false
#define SINGLE_CHANNEL_GW_IQ_RX						false
#define SINGLE_CHANNEL_GW_TX_TIMEOUT				15000
#define SINGLE_CHANNEL_GW_RX_TIMEOUT				0
/* END */

#ifdef __cplusplus
}
#endif

#endif // __LORAMAC_BOARD_H__
