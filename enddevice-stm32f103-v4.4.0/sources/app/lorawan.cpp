#include <string.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_io.h"
#include "../common/xprintf.h"

#include "app.h"
#include "app_dbg.h"
#include "task_list.h"
#include "app_bsp.h"

#include "loramac/radio/sx1276/sx1276_cfg.h"
#include "loramac/radio/sx1276/sx1276.h"

#include "loramac/mac/LoRaMac-definitions.h"
#include "loramac/mac/LoRaMac.h"
#include "loramac/mac/LoRaMacCrypto.h"

#include "loramac/crypto/aes.h"
#include "loramac/crypto/cmac.h"

#include "lorawan.h"

//#define OVER_THE_AIR_ACTIVATION

#ifdef  OVER_THE_AIR_ACTIVATION
#define LORAWAN_APPLICATION_EUI				{ 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA }
#define LORAWAN_APPLICATION_KEY				{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 }
static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;
#else

#define LORAWAN_NETWORK_ID					0
#define LORAWAN_NWKSKEY						{ 0xcb ,0xc1, 0xfc, 0xc4, 0x88, 0x61, 0x69, 0x86, 0x16, 0x6a, 0xfd, 0x40, 0xfb, 0x3f, 0xf9, 0x75 }
#define LORAWAN_APPSKEY						{ 0xdd ,0x0f ,0xea ,0x6e ,0x6a ,0xc3 ,0xba ,0xe6 ,0xe7 ,0x3e ,0xe6 ,0xa9 ,0xce ,0x19 ,0x56 ,0x99 }

static uint32_t	LorawanDevAddress;
static const uint32_t	NwkID		= LORAWAN_NETWORK_ID;
static const uint8_t	NwkSKey[]	= LORAWAN_NWKSKEY;
static const uint8_t	AppSKey[]	= LORAWAN_APPSKEY;
#endif

static uint8_t LorawanDevEui[8];

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static void McpsConfirm( McpsConfirm_t *mcpsConfirm );
static void McpsIndication( McpsIndication_t *mcpsIndication );
static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );

static char* send_msg = (char*)"hello!";

void lorawan_init() {
	APP_DBG("lorawan_init()\n");

	LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
	LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
	LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
	LoRaMacCallbacks.GetBatteryLevel = 0;
	LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU433 );

	mibReq.Type = MIB_ADR;
	mibReq.Param.AdrEnable = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_REPEATER_SUPPORT;
	mibReq.Param.EnableRepeaterSupport = false;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_PUBLIC_NETWORK;
	mibReq.Param.EnablePublicNetwork = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEVICE_CLASS;
	mibReq.Param.Class = CLASS_C;
	LoRaMacMibSetRequestConfirm( &mibReq );

	LorawanDevAddress = get_device_id();

	get_device_Eui(LorawanDevEui);
	APP_DBG("DevEui:");
	for(int i = 0; i < 8; i++)	APP_DBG("0x%02X ", LorawanDevEui[i]);
	APP_DBG("\n");
}

void lorawan_join() {
	APP_DBG("lorawan_join()\n");

#ifdef OVER_THE_AIR_ACTIVATION
	MlmeReq_t mlmeReq;

	mlmeReq.Type = MLME_JOIN;
	mlmeReq.Req.Join.DevEui = LorawanDevEui;
	mlmeReq.Req.Join.AppEui = AppEui;
	mlmeReq.Req.Join.AppKey = AppKey;
	mlmeReq.Req.Join.NbTrials = 3;

	LoRaMacMlmeRequest( &mlmeReq );

#else

	mibReq.Type = MIB_NET_ID;
	mibReq.Param.NetID = NwkID;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_DEV_ADDR;
	mibReq.Param.DevAddr = LorawanDevAddress;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NWK_SKEY;
	mibReq.Param.NwkSKey = (uint8_t*)NwkSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_APP_SKEY;
	mibReq.Param.AppSKey = (uint8_t*)AppSKey;
	LoRaMacMibSetRequestConfirm( &mibReq );

	mibReq.Type = MIB_NETWORK_JOINED;
	mibReq.Param.IsNetworkJoined = true;
	LoRaMacMibSetRequestConfirm( &mibReq );

	APP_DBG("[APB] DevAddr:0x%08X\n", LorawanDevAddress);

#endif

	timer_set(AK_TASK_UI_ID, AK_LORAWAN_SENT_TEST, 1000, TIMER_ONE_SHOT);
}

void lorawan_send() {
	APP_DBG("lorawan_send()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			McpsReq_t mcpsReq;
			LoRaMacTxInfo_t txInfo;

			if( LoRaMacQueryTxPossible( strlen(send_msg), &txInfo ) != LORAMAC_STATUS_OK )
			{
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = 0;
				mcpsReq.Req.Unconfirmed.fBuffer = 0;
				mcpsReq.Req.Unconfirmed.fBufferSize = 0;
				mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;

//				mcpsReq.Type = MCPS_CONFIRMED;
//				mcpsReq.Req.Confirmed.fPort = 0;
//				mcpsReq.Req.Confirmed.fBuffer = 0;
//				mcpsReq.Req.Confirmed.fBufferSize = 0;
//				mcpsReq.Req.Confirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
//				mcpsReq.Req.Confirmed.NbTrials = 0;
			}
			else {
				mcpsReq.Type = MCPS_UNCONFIRMED;
				mcpsReq.Req.Unconfirmed.fPort = APP_PORT;
				mcpsReq.Req.Unconfirmed.fBuffer = send_msg;
				mcpsReq.Req.Unconfirmed.fBufferSize = strlen(send_msg);
				mcpsReq.Req.Unconfirmed.Datarate = LORAWAN_DEFAULT_DATARATE;

//				mcpsReq.Type = MCPS_CONFIRMED;
//				mcpsReq.Req.Confirmed.fPort = 0;
//				mcpsReq.Req.Confirmed.fBuffer = 0;
//				mcpsReq.Req.Confirmed.fBufferSize = 0;
//				mcpsReq.Req.Confirmed.Datarate = LORAWAN_DEFAULT_DATARATE;
//				mcpsReq.Req.Confirmed.NbTrials = 2;
			}

			if(LoRaMacMcpsRequest( &mcpsReq ) == LORAMAC_STATUS_OK) {
			}

		}
		else {
			APP_DBG("Joined not yet\n");
		}
	}
}

void lorawan_leave() {
	APP_DBG("lorawan_leave()\n");

	MibRequestConfirm_t mibReq;
	LoRaMacStatus_t status;

	mibReq.Type = MIB_NETWORK_JOINED;
	status = LoRaMacMibGetRequestConfirm( &mibReq );

	if( status == LORAMAC_STATUS_OK ) {
		if( mibReq.Param.IsNetworkJoined == true ) {

			mibReq.Type = MIB_DEV_ADDR;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {

			}
		}
		else {
			APP_DBG("Joined not yet\n");

		}
	}
}


static void McpsConfirm( McpsConfirm_t *mcpsConfirm ) {
	if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
		switch( mcpsConfirm->McpsRequest ) {
		case MCPS_UNCONFIRMED: {
			// Check Datarate
			// Check TxPower
			APP_DBG("McpsConfirm-MCPS_UNCONFIRMED\n");

			timer_set(AK_TASK_UI_ID, AK_LORAWAN_SENT_TEST, 5000, TIMER_ONE_SHOT);

			break;
		}
		case MCPS_CONFIRMED: {
			// Check Datarate
			// Check TxPower
			// Check AckReceived
			// Check NbTrials
			APP_DBG("McpsConfirm-MCPS_CONFIRMED\n");

			//timer_set(AK_TASK_UI_ID, AK_LORAWAN_SENT_TEST, 5000, TIMER_ONE_SHOT);

			break;
		}
		case MCPS_PROPRIETARY: {
			APP_DBG("McpsConfirm-MCPS_PROPRIETARY\n");
			break;
		}
		default:
			break;
		}

	}
}

static void McpsIndication( McpsIndication_t *mcpsIndication ) {
	if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK ) {
		return;
	}

	switch( mcpsIndication->McpsIndication ) {
	case MCPS_UNCONFIRMED: {
		APP_DBG("McpsIndication-MCPS_UNCONFIRMED\n");
		break;
	}
	case MCPS_CONFIRMED: {
		APP_DBG("McpsIndication-MCPS_CONFIRMED\n");
		break;
	}
	case MCPS_PROPRIETARY: {
		APP_DBG("McpsIndication-MCPS_PROPRIETARY\n");
		break;
	}
	case MCPS_MULTICAST: {
		APP_DBG("McpsIndication:MCPS_MULTICAST\n");
		break;
	}
	default:
		break;
	}

	if( mcpsIndication->RxData == true ) {
		APP_DBG("McpsIndication-RxData:true\n");
	}
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm ) {
	switch( mlmeConfirm->MlmeRequest ) {
	case MLME_JOIN: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			// Status is OK, node has joined the network
			APP_DBG("MlmeConfirm-MLME_JOIN SUCCESS\n");

			MibRequestConfirm_t mibReq;
			mibReq.Type = MIB_DEV_ADDR;
			if( LoRaMacMibGetRequestConfirm( &mibReq ) == LORAMAC_STATUS_OK) {
				APP_DBG("[OTA] DevAddr:0x%08X\n", mibReq.Param.DevAddr);
			}
		}
		else {
			// Join was not successful. Try to join again
			APP_DBG("MlmeConfirm-MLME_JOIN FAIL\n");
			lorawan_join();
		}
		break;
	}
	case MLME_LINK_CHECK: {
		if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK ) {
			// Check DemodMargin
			// Check NbGateways
			APP_DBG("MlmeConfirm-MLME_LINK_CHECK\n");
		}
		break;
	}
	default:
		break;
	}

}
