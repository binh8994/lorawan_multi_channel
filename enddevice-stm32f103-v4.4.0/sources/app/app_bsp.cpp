#include "app.h"
#include "app_dbg.h"
#include "app_bsp.h"
#include "task_list.h"
#include "lorawan.h"

#include "../ak/message.h"
#include "../driver/button/button.h"

button_t button;

void button_callback(void* b) {
	button_t* me_b = (button_t*)b;
	switch (me_b->state) {
	case BUTTON_SW_STATE_RELEASE: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_RELEASE\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_HOLD_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_SHORT_HOLD_PRESS\n");
	}
		break;

	case BUTTON_SW_STATE_SHORT_RELEASE_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_SHORT_RELEASE_PRESS\n");

		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, AK_BUTTON_ONE_CLICK);
		task_post(AK_TASK_UI_ID, smsg);

	}
		break;

	case BUTTON_SW_STATE_LONG_PRESS: {
		APP_DBG("[btn_mode_callback] BUTTON_SW_STATE_LONG_PRESS\n");

		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, AK_BUTTON_LONG_PRESS);
		task_post(AK_TASK_UI_ID, smsg);

	}
		break;

	default:
		break;
	}
}

