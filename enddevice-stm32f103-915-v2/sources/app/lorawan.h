#ifndef __LORAWAN_H__
#define __LORAWAN_H__

#define APP_PORT						80
#define DATA_F_SIZE						1
#define ZCL_CLUSTER_ID_GEN_ON_OFF		0x0006
#define ZCL_DATATYPE_UINT8				0x20
#define ZCL_CMD_ID_DEFAULT_RESPONSE		0x0B
#define ZCL_CMD_ID_CONTROL				0x01
#define ZCL_CMD_ID_REPORT				0x0A

typedef struct {
	uint16_t cluster_id;
	uint32_t device_addr;
	uint16_t sequence;
	uint8_t cmd_id;
	uint8_t data_type;
	uint8_t data_len;
	uint8_t data[DATA_F_SIZE];
} __attribute__((__packed__)) lorawan_msg_t;

typedef struct {
	uint8_t device_Eui[8];
	uint32_t device_addr;
} __attribute__((__packed__)) lorawan_leave_msg_t;

extern void lorawan_init();
extern void lorawan_join();
extern void lorawan_leave();
extern void lorawan_send();

#endif // __LORAWAN_H__
