#include <SoftwareSerial.h>

#define LED 13
SoftwareSerial LoraSerial(5, 6); // RX, TX

//String devaddr = "017979FD";
//String nwkskey = "01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16";
//String appskey = "01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16";

String devaddr = "26041A31";
String nwkskey = "7FA0B1EBFC36CB91EDC821D1FCD495A3";
String appskey = "36509D4C207AA9B759C14D50261E9E1A";

String response;
String cmd;

void setup() {
  Serial.begin(9600);
  while (!Serial);
  LoraSerial.begin(9600);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, 0);

  while(LoraSerial.available()) LoraSerial.read();
  
}

void loop() {

  while(LoraSerial.available()) LoraSerial.read();
  
  digitalWrite(LED, 1);

  cmd = "AT+MODE=TEST";
  sendRawcmd(cmd);

  delay(100);
  
  cmd = "AT+TEST=RFCFG, 923.6, SF10, 125, 8, 8, 14, ON, OFF, ON";
  sendRawcmd(cmd);

  delay(100);

  cmd = "AT+TEST=?";
  sendRawcmd(cmd);

  delay(100);

  cmd = "AT+TEST=TXLRPKT, \"12345678\"";
//  cmd = "AT+TEST=RXLRPKT";
  sendRawcmd(cmd);

  digitalWrite(LED, 0);
  
  delay(15000);

//  while(1) {
//    String r = LoraSerial.readStringUntil('\n');  
//    Serial.println(r);
//    delay(100);
//  }
  
}

String sendRawcmd(String s) {
  LoraSerial.println(s);
  String r = LoraSerial.readStringUntil('\n');
  Serial.println(r);
  while(LoraSerial.available()) LoraSerial.read();
    return r;
}

