#!/bin/bash

echo ""
echo "########################"
echo "[check_run_wvdial] begin"
date
echo "########################"

#14b5->14a8

if [ ! "$( pgrep "wvdial")" ]; then

if [ ! "$( lsusb | grep "12d1:14a8")" ]; then
echo "[check_run_wvdial] usb_modeswitch -v 12d1 -p 14b5 -J"
usb_modeswitch -v 12d1 -p 14b5 -J

echo "[check_run_wvdial] sleep 10s"
sleep 10s

fi


echo "[check_run_wvdial] wvdial 3gviettel &"
wvdial 3gviettel &

echo "[check_run_wvdial] sleep 2s wait for start wvdial"

fi
