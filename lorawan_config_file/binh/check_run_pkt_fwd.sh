#!/bin/bash

echo ""
echo "########################"
echo "[check_run_pkt_fwd] begin"
date
echo "########################"

if [ ! "$( pgrep "lora_pkt_fwd")" ]; then
echo "[check_run_pkt_fwd] cd /root/scripts"
cd /root/scripts

echo "[check_run_pkt_fwd] ./update_gwid.sh global_conf.json"
./update_gwid.sh global_conf.json

echo "[check_run_pkt_fwd] ./reset_lgw.sh start 21"
./reset_lgw.sh start 21

echo "[check_run_pkt_fwd] ./lora_pkt_fwd"
./lora_pkt_fwd &

fi
