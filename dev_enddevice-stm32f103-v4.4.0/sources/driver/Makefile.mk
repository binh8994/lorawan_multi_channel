CFLAGS		+= -I./sources/driver/led
CFLAGS		+= -I./sources/driver/button

VPATH += sources/driver/led
VPATH += sources/driver/button

SOURCES += sources/driver/led/led.c
SOURCES += sources/driver/button/button.c

